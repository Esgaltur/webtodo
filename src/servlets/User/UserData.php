<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 24.08.2015
 * Time: 00:30
 */

namespace webtodo;


include_once('UserDAO.php');
class UserData
{

    public function InsertUserData()
    {
        try {
            $dao = new UserDAO();
            $dao->InsertUserDatas($this);
        }
        finally{
            $dao->CloseAndNilConn();
        }
    }
    /**
     * UserData constructor.
     * @param $name
     * @param $email
     * @param $dateOfBirth
     */
    public function __construct($id,$name, $email, $dateOfBirth)
    {
        $this->id = $id;
        $this->fName = $name;
        $this->email = $email;
        $this->dateOfBirth = $dateOfBirth;
    }


    /**
     * @return Name of User
     */
    public function getFName()
    {
        return $this->fName;
    }

    /**
     * @param Setter for User name
     */
    public function setFName($fName)
    {
        $this->fName = $fName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    private $fName;
    private $email;
    private $dateOfBirth;
}