<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 22.08.2015
 * Time: 17:11
 */

namespace webtodo;
include_once('UserDAO.php');
class User
{

    public function __construct() {
        $get_arguments       = func_get_args();
        $number_of_arguments = func_num_args();

        if (method_exists($this, $method_name = '__construct'.$number_of_arguments)) {
            call_user_func_array(array($this, $method_name), $get_arguments);
        }
    }
    function __construct2($login,$password)
    {
       $this->login = $login;
       $this->password = $password;
    }
    function __construct3($id,$login,$password)
    {
        $this->login = $login;
        $this->password = $password;
        $this->id = $id;
        $this->lastSuccLog = time();
    }
    function __construct4($id,$login,$password,$lastSuccLog)
    {
        $this->login = $login;
        $this->password = $password;
        $this->id = $id;
        $this->lastSuccLog = $lastSuccLog;
    }
    function Insert()
    {
        try {
        $user = new UserDAO();
        $user->InsertUser($this);
    }
    finally{
        $user->CloseAndNilConn();
        unset($user);}
    }
    function InsertDatas()
    {
        $auth = new UserDAO;
        $auth->InsertUserDatas($this);
        $auth->CloseAndNilConn();
        unset($auth);
    }
    function SetLastSuccLogIntoDB()
    {
        $auth = new UserDAO;
        $auth->SetLastSuccLog($this);
        $auth->CloseAndNilConn();
        unset($auth);
    }


    /**
     * @return mixed
     */
    public function getLastSuccLog()
    {
        return $this->LastSuccLog;
    }

    /**
     * @param mixed $LastSuccLog
     */
    public function setLastSuccLog($LastSuccLog)
    {
        $this->LastSuccLog = $LastSuccLog;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
function toString()
{
    return $this->id.' '.$this->login.' '.$this->password.' '.$this->LastSuccLog;
}
  private $id;
  private $login;
  private $password;
  private $LastSuccLog;

}