<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 22.08.2015
 * Time: 16:43
 */

namespace webtodo;

include_once('../../servlets/DAO/DAO_Base.php');
include_once('User.php');
Use \webtodo\User;
Use \webtodo\DAO;

class UserDAO extends DAO
{
    /**
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param \webtodo\User $user
     * @return bool
     */
    function InsertUserAuth(User $user)
    {
        $sqlInsert = "INSERT INTO userauth (`login`, `password`,`lastSuccLog`)
           VALUES ('" . $user->getLogin() . "','" . md5($user->getPassword()) . "',FROM_UNIXTIME(" . time() . "))";

        if ($this->conn->query($sqlInsert) === TRUE) {
            return true;
        } else {
            echo "Error: " . $sqlInsert . "<br>" . $this->conn->error;
            return false;
        }
    }

    /**
     * @param \webtodo\User $user
     * @return bool
     */
    function UpdateUserAuth(User $user)
    {
        try {
            $sqlUpdate = "UPDATE userauth SET login= " . $user->getLogin() .
                " password= " . $user->getPassword() . "lastSuccLog=" . time() . " WHERE id=" . $user->getId();

            if ($this->conn->query($sqlUpdate) === TRUE) {
                return true;
            } else {
                echo "Error updating record: " . $this->conn->error;
                return false;
            }
        } finally {
            $this->CloseAndNilConn();
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public static function SetLastSuccLog($id)
    {
        try {
            $dao = new UserDAO();
            $sqlUpdate = "UPDATE userauth SET lastSuccLog=FROM_UNIXTIME("
                . time() . ") WHERE id=" . $id;

            if ($dao->conn->query($sqlUpdate) === TRUE) {

                return true;
            } else {
                echo "Error updating last success login: " . $dao->conn->error;
                return false;
            }
        } finally {
            $dao->CloseAndNilConn();
        }

    }

    /**
     * @param $id
     * @return bool|\webtodo\User
     */
    public static function getUserById($id)
    {
        try {
            $dao = new UserDAO();
            $sqlGetByID = "SELECT* FROM userauth WHERE id=" . $id;
            $result = $dao->conn->query($sqlGetByID);
            $res_acc = $result->fetch_assoc();

            if ($res_acc) {
                return
                    new User($res_acc['id'],
                        $res_acc['login'],
                        $res_acc['password'],
                        $res_acc['lastSuccLog']);
            } else return false;
        } finally {
            $dao->CloseAndNilConn();
        }
    }

    /**
     * @param $id
     * @return bool
     */
    function DeleteUserById($id)
    {
        try {
            $sqldelete = "DELETE from auth WHERE id=" . $id;

            if ($this->conn->query($sqldelete) === TRUE) {
                return true;
            } else {
                echo "Error deleting record: " . $this->conn->error;
                return false;
            }
        } finally {
            $this->CloseAndNilConn();
        }
    }

    /**
     * @param $login
     * @param $password
     * @return bool
     */
    public static function AuthUser($login, $password)
    {
        $dao = null;
        try {
            if (isset($login) and isset($password)) {
                $dao = new UserDAO();
                $sqlGetByID = "SELECT* FROM userauth WHERE login='" . $login .
                    "' AND password='" . $password . "'";
                $result = $dao->conn->query($sqlGetByID);
                $resAcc = $result->fetch_assoc();
                unset($_SESSION['name']);
                unset($_SESSION['usrId']);

                $_SESSION['name'] = $dao->getFullUserFLNameById($resAcc['id']);
                $_SESSION['usrId'] = $resAcc['id'];

                if (isset($resAcc)) {
                    $dao->SetLastSuccLog($resAcc['id']);
                    return
                        true;
                } else
                    echo "Login or password is not correct";
                return false;
            }
        } finally {
            $dao->CloseAndNilConn();
        }
    }

    /**
     *
     */
    function CloseAndNilConn()
    {
        $this->conn->close();
        $this->conn = null;
    }


    /********************************************************************
     *
     * UserData methods class
     **********************************************************************/
    /**
     * @param UserData $userData
     * @return bool
     */
    function InsertUserDatas(UserData $userData)
    {
        try {
            $sqlInsert = "INSERT INTO userdata(`usrId`,`name`, `email`,`DBirth`)
           VALUES ('" . $userData->getId() . "','" . $userData->getFName() .
                "','" . $userData->getEmail() .
                "','" . $userData->getDateOfBirth() . "')";


            if ($this->conn->query($sqlInsert) === TRUE) {
                return true;
            } else {
                echo "Error: " . $sqlInsert . "<br>" . $this->conn->error;
                return false;
            }
        } finally {
            $this->CloseAndNilConn();
        }
    }

    /**
     * @param $id
     * @return string
     */
    public static function getFullUserFLNameById($id)
    {
        $dao = null;
        try {
            $dao = new UserDAO();
            if (isset($id)) {
                $sqlGetByID = "SELECT* FROM `userauth` INNER JOIN `userdata` ON
                   `userauth`.`id`=`userdata`.`UsrId` WHERE `userauth`.`id`=".$id;
                $_SESSION['sql'] = $sqlGetByID;
                $result = $dao->conn->query($sqlGetByID);
                $res_acc = $result->fetch_assoc();
                if ($res_acc) {
                    if (isset($res_acc['name'])) {
                        return $res_acc['name'];
                    } else {
                        return 'New user';
                    }
                } else
                    return "Guest";
            }
        } finally {
            $dao->CloseAndNilConn();
        }

    }
}