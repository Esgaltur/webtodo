<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 22.08.2015
 * Time: 16:43
 */
namespace webtodo;

include_once('../User/UserDAO.php');
include_once('../../../SharedFunctions.php');

Use webtodo\UserDAO;

$badLogin ='';
if(isset($_POST["login"]) and isset($_POST["password"])) {
    $login = StripAndTrim($_POST["login"]);
    $password = StripAndTrim($_POST["password"]);

    if (UserDAO::AuthUser($login,$password)) {




        header('Location:../../../web/panel/');
    } else {
        $badLogin = md5("badLogin".time());
        if(!isset($_SESSION)){ session_start();}
        unset($_SESSION["bad"]);
        $_SESSION["bad"] =  $badLogin;
        header('Location:./../../../web/auth/?act='.$badLogin);
    }
}
