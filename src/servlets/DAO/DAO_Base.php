<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 23.08.2015
 * Time: 14:23
 */

namespace webtodo;

class DAO
{
    function __construct()
    {
        $this->conn = $this->getConnection();

    }
    function __destruct(){
       unset($this->conn);
    }
    function getConnection()
    {
        $connection = new \mysqli($this->dbHost, $this->dbLogin, $this->dbPassword, $this->dbName);
        if ($connection->connect_error)
        {
            echo "Failed to connect to MySQL: " . $connection->connect_error;
        }
        if (! $connection->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n",  $connection->error);
            exit();
        }
        return $connection;
    }

    protected $conn = null;
    private $dbName = "webtodo.com";
    private $dbLogin = "root";
    private $dbPassword = "doithard";
    private $dbHost = "localhost";
}
