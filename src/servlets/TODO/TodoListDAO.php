<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 06.09.2015
 * Time: 20:19
 */

namespace webtodo;

include_once($_SERVER['DOCUMENT_ROOT'] . '/webtodo/src/servlets/DAO/DAO_Base.php');
//include_once('servlets/DAO/DAO_Base.php');
include_once('TodoList.php');
Use \webtodo\TodoList;
Use \webtodo\DAO;


class TodoListDAO extends DAO
{
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    function InsertNewList(TodoList $List)
    {
        $InsertSQL =
            'INSERT INTO todolist(`ListName`,`CreationDate`,`userId`) ' .
            'VALUES("' . $List->getListName() .'",FROM_UNIXTIME('.time().'),' . $List->getUserId() . ')';
        if ($this->conn->query($InsertSQL) === TRUE) {
            return true;
        } else {
            echo "Error: " . $InsertSQL . "<br>" . $this->conn->error;
            return false;
        }
    }
    function DeleteListById($id)
    {   $DeleteTodoSQL = 'DELETE FROM `todo` WHERE `TodoListId`='.$id;
        $DeleteSQL =
            'DELETE FROM `todolist` WHERE `listID`='.$id;

        if ($this->conn->query($DeleteSQL) === TRUE) {
            $dao = new TodoDAO();
            $dao->DeleteTODOByListId($id);
        } else {
            echo "Error: " . $DeleteSQL . "<br>" . $this->conn->error;
            return false;
        }
    }

    public static function  getAllListsByUsrId($Id)
    {
        $result = null;
        $SelectAllSQL = 'SELECT * FROM `todolist` WHERE userId=' . $Id;
        try {
            $dao = new TodoListDAO();
            if ($dao->conn->query($SelectAllSQL) == true) {
                $result = $dao->conn->query($SelectAllSQL);
                return $result;
            } else return 'No datas';
        } finally {
            $dao->conn->close();
            unset($dao);
        }
    }

    public static function getTodoListById($userID, $listID)
    {

        $result = null;
        $sqlTodoSList = 'SELECT * FROM `todo`
         INNER JOIN `todolist` ON `todolist`.`listID`=`todo`.`TodoListId`
         WHERE `TodoListId`='
            . $listID
            . ' AND `usrId`= ' . $userID;
        try {
            $dao = new TodoListDAO();
            $TodoList = new TodoList();
            $TodoList->setListID($listID);
            $TodoList->setUserId($userID);
            if ($dao->conn->query($sqlTodoSList) == true) {
                $result = $dao->conn->query($sqlTodoSList);

                while ($row = $result->fetch_assoc()) {
                    $TodoList->Add(
                        new Todo(
                            $row['RecId'], $row['usrId'], $row['name'], $row['text'],
                            $row['category'], $row['level'], $row['from'],
                            $row['to'], $row['TodoListId']));

                    if (empty($TodoList->getListName())) {
                        $TodoList->setListName($row['ListName']);
                    }
                    if (empty($TodoList->getCreationDate())) {
                        $TodoList->setCreationDate($row['CreationDate']);
                    }
                    if (empty($TodoList->getListID())) {
                        $TodoList->setCreationDate($row['TodoListId']);
                    }
                }
                return $TodoList;
            } else {
                echo "Error sql : " . $sqlTodoSList;
            };
        } finally {
            $dao->conn->close();
            $dao->conn = null;

        }
    }


}