<?php
/**
 * Created by PhpStorm.
 * User: Dmitrij Sosnovi�
 * Date: 15.9.2015
 * Time: 15:45
 */
namespace webtodo;
include_once('./TodoListDAO.php');
include_once('./TodoList.php');
include_once('../../../SharedFunctions.php');

use webtodo\TodoListDAO;
try
{
    $dao = new TodoListDAO();
    $list = new TodoList();
    if(isset($_POST['listName'])&&isset($_POST['usrID'])&&$_POST['listName']!=''){
        $list->setListName(StripAndTrim($_POST['listName']));
        $list->setUserId(StripAndTrim($_POST['usrID']));
        if($dao->InsertNewList($list)){
            header('Location:../../../web/panel/');
        }
        else{
           header('Location:'.$_SERVER['HTTP_REFERER'].'?act='.md5('badlistnameorusrid'));
    }
    }
}
finally{
    unset($dao);
    unset($list);
}

