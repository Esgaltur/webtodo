<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 06.09.2015
 * Time: 19:53
 */

namespace webtodo;
/***
 * Levels oftodo
 */

/**
 * Category
 */

class TODO
{


    /**
     * @return mixed
     */
    public function getRecId()
    {
        return $this->RecId;
    }

    /**
     * @return mixed
     */
    public function getUsrId()
    {
        return $this->UsrId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->Text;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }


    /**
     * TODO constructor.
     * @param $UsrId
     * @param $Name
     * @param $Text
     * @param $category
     * @param $level
     * @param $from
     * @param $to
     */
    public function __construct(
        $RecId,$UsrId, $Name, $Text, $category, $level, $from, $to,$ListID)
    {

        $this->RecId = $RecId;
        $this->UsrId = $UsrId;
        $this->Name = $Name;
        $this->Text = $Text;
        $this->category = $category;
        $this->level = $level;
        $this->from = $from;
        $this->to = $to;
        $this->ListID = $ListID;
    }



    private $RecId;
    private $UsrId;
    private $Name;
    private $Text;

    /**
     * @return mixed
     */
    public function getListID()
    {
        return $this->ListID;
    }


    private $category;
    private $level;
    private $from;
    private $to;
    private $ListID;
}