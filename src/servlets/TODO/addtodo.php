<?php
namespace webtodo;
include_once "TodoDAO.php";
include_once "TODO.php";
include_once "../../../SharedFunctions.php";
Use \webtodo\TodoDAO;
Use webtodo\TODO;
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 14.09.2015
 * Time: 00:26
 */
/***
 * <form style="margin-left:50%" action="../../../../src/servlets/TODO/addtodo.php" method="post">
<input type="text" name="TodoName" placeholder="Todo name"><br>
<textarea name="text" placeholder="Enter text/points to do">

</textarea><br>
<input type="date" name="from" placeholder="from"><br>
<input type="date" name="to" placeholder="to"><br>
<select name="category" >
<option value="1">Other</option>
</select><br>
<input type="text" name="level" placeholder="level"><br>
<input type="text" name="todolistid" hidden="hidden" value="<?php echo $_SESSION['usrId']?>">
<input type="submit" value="Insert new Todo to list">

</form>
 */
try
{
    $dao = new TodoDAO();
    $todo  = null;
    if(isset($_POST['TodoName'])&&
        isset($_POST['text'])    &&
        isset($_POST['from'])    &&
        isset($_POST['to'])      &&
        isset($_POST['category'])&&
        isset($_POST['level'])   &&
        isset($_POST['todolistid']) &&
        isset($_POST['usrid'])
    ){
        $todoname = StripAndTrim($_POST['TodoName']);
    $text = StripAndTrim(  $_POST['text'] );
        $from =   StripAndTrim( $_POST['from']);
            $to =   StripAndTrim( $_POST['to'] );
                $category =   StripAndTrim( $_POST['category']);
                    $level =   StripAndTrim( $_POST['level']   );
                        $listid =    StripAndTrim($_POST['todolistid'] );
    $usrid =  StripAndTrim($_POST['usrid'] );
    $todo = new TODO(null,$usrid,$todoname,$text,$category,$level,$from,$to,$listid);

    if($dao->InsertTODO($todo)){
       header('Location:../../../web/panel/todo/');
    }
}
}
finally{
   unset($dao);
    unset($todo);
}


