<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 06.09.2015
 * Time: 20:00
 */

namespace webtodo;
include_once('TODO.php');
Use webtodo\TODO;

class TodoList
{

    /**
     * TodoList constructor.
     * @param $List - kopirovaci konstruktor
     *
     */
    /*  public function __construct() {
         $get_arguments       = func_get_args();
         $number_of_arguments = func_num_args();

         if (method_exists($this, $method_name = '__construct'.$number_of_arguments)) {
             call_user_func_array(array($this, $method_name), $get_arguments);
         }
     }
 public function __construct1(TodoList $List)
     {
         $this ->List = new SplObjectStorage;
         $this->List->addAll($List->getList());
         $this->ListID = $List->getListID();
         $this->ListName = $List->getListName();
         $this->CreationDate = $List->getCreationDate();
         $this->UserId = $List->getUserId();
     }*/
    public function __construct()
    {
        $this->List = array();

    }

    /**
     * TodoList destructor.
     */
    public function __destruct()
    {
        unset($this->List);
    }

    function getCount()
    {

        if (count($this->List) > 0)
            return count($this->List);
        else {
            echo "Error count " . count($this->List);
            return count($this->List);
        }
    }

    function Add(TODO $aItem)
    {
        $this->List[] = $aItem;
    }

    /**
     * @return mixed
     */
    public function getListID()
    {
        return $this->ListID;
    }

    /**
     * @return mixed
     */
    public function getListName()
    {
        return $this->ListName;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->CreationDate;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->UserId;
    }

    /**
     *
     */
    public function getList()
    {
        return $this->List;
    }

    public function Clear()
    {
        unset($this->List);
        $this->List = null;
        $this->List = array();
    }

    private $ListID;

    /**
     * @param mixed $ListID
     */
    public function setListID($ListID)
    {
        $this->ListID = $ListID;
    }

    /**
     * @param mixed $ListName
     */
    public function setListName($ListName)
    {
        $this->ListName = $ListName;
    }

    /**
     * @param mixed $CreationDate
     */
    public function setCreationDate($CreationDate)
    {
        $this->CreationDate = $CreationDate;
    }

    /**
     * @param mixed $UserId
     */
    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

    /**
     * @param null $List
     */
    public function setList(TodoList $List)
    {
        $this->Clear();
        $this->List = array_fill(0, $List->getCount(), $List->getList());
    }

    public function printList()
    {
        /**
         *         new Todos(
         * $row['RecId'],$row['usrId'],$row['name'],$row['text'],
         * $row['category'],$row['level'],$row['from'],
         * $row['to'],$row['TodoListId']));
         */
        echo "  <tr>
                    <td>
                        <table>";
                                if (!empty($this->getList())) {
                                    echo "
                                          <tr>
                                            <th>List name:</th>
                                            <th>Creation Date :</th>
                                            <th>Add new Todo</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                          </tr>";
                                }
                                if (!empty($this->getList())) {
                                    echo "<tr>
                                              <td>" . $this->getListName() . "</td>
                                              <td>" . $this->getCreationDate() . "</td>";
                                }
                             echo "<td><a href='../../../../webtodo/web/panel/todo/add/?tdlid=".$this->getListID()."'> Add new Todo</a></td>
                                   <td><a href='../../../../webtodo/web/panel/todo/edit/?tdlid=".$this->getListID()."'> Edit List</a></td>
                                   <td><a href='../../../../webtodo/src/servlets/TODO/deletetdl.php?tdlid=".$this->getListID()."'> Delete  List</a></td>
                                          </tr>
                        </table>
                    </td>
                </tr>";
        $PrintedList = $this->getList();
        if (!empty($PrintedList)) {
            echo "<tr>
                      <th>Name of Todo</th>
                      <th>Points to do</th>
                      <th>Category</th>
                      <th>Level</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Edit</th>
                      <th>Delete</th>
               </tr>";
            try {
                for ($i = 0; $i < $this->getCount(); $i++) {
                    echo
                        "<tr>
                    <td>" . $PrintedList[$i]->getName() . "</td>
                    <td>" . $PrintedList[$i]->getText() . "</td>
                    <td>" . $PrintedList[$i]->getCategory() . "</td>
                    <td>" . $PrintedList[$i]->getLevel() . "</td>
                    <td>" . $PrintedList[$i]->getFrom() . "</td>
                    <td>" . $PrintedList[$i]->getTo() . "</td>
                    <td><a href='./edit/?rid=" . $PrintedList[$i]->getRecId() . "&uid=" .
                        $PrintedList[$i]->getUsrId() . "&tdlid=" . $PrintedList[$i]->getListId() . "'>Edit Todo</a></td>
                            <td><a href='../../../src/servlets/TODO/deleteTodo.php?rid="
                        . $PrintedList[$i]->getRecId()."'>Delete</a></td>
                </tr>";
                }

            } finally {
                unset($PrintedList);
            }
        } else {
            echo'No data';
        }
    }

    private $ListName;
    private $CreationDate;
    private $UserId;
    private $List = null;
}
