<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 06.09.2015
 * Time: 19:54
 */

namespace webtodo;
include_once("../DAO/DAO_Base.php");
use webtodo\TODO;

class TodoDAO extends DAO
{
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }
    function InsertTODO(Todo $List)
    {

        $InsertSQL =
            'INSERT INTO `todo`(`usrid`,`name`,`text`,`from`,`to`,`level`,`category`,`TodoListId`) '
        .'VALUES('.$List->getUsrId().',"'.$List->getName().'","'.$List->getText().'","'.
            $List->getFrom().'","'.$List->getTo().'",'.$List->getLevel().','.$List->getCategory().','.
            $List->getListID().')';
        if ($this->conn->query($InsertSQL) === TRUE) {
            return true;
        } else {
            echo "Error: " . $InsertSQL . "<br>" . $this->conn->error;
            return false;
        }
    }
    function UpdateTODO(Todo $List)
    {
        $UpdateSQL =
            'UPDATE `todo` SET '.
            '`name`='.$List->getName().',`text`='.$List->getText().
            ',`from`='.$List->getFrom().',`to`='.$List->getTo().
            ',`level`='.$List->getLevel().',`category`='.$List->getCategory().
            ' WHERE `usrid`='.$List->getUsrId().' AND `TodoListId`='.$List->getListID();
        if ($this->conn->query($UpdateSQL) === TRUE) {
            return true;
        } else {
            echo "Error: " . $UpdateSQL . "<br>" . $this->conn->error;
            return false;
        }
    }
    function DeleteTODOById($id)
    {
        $UpdateSQL =
            'DELETE FROM `todo` WHERE `RecId`='.$id;
        if ($this->conn->query($UpdateSQL) === TRUE) {
            return true;
        } else {
            echo "Error: " . $UpdateSQL . "<br>" . $this->conn->error;
            return false;
        }
    }
    function DeleteTODOByListId($id)
    {
        $DeleteSQL =
            'DELETE FROM `todo` WHERE `TodoListId`='.$id;
        if ($this->conn->query($DeleteSQL) === TRUE) {
            return true;
        } else {
            echo "Error: " . $DeleteSQL . "<br>" . $this->conn->error;
            return false;
        }
    }



}