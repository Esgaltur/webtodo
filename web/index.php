
<!DOCTYPE html>
<html>
<head>
<?php unset($_SESSION)?>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>WebToDo.com</title>
    <!--Styles--->
    <link rel="stylesheet" href="css/indexStyle.css">
    <link rel="stylesheet" href="css/menu.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/footer.css">
    <!--Styles--->
    <!--JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/menu.js"></script>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();
            });
        }) (jQuery);
    </script>
    <!--JavaScript--->
</head>
<body>
<?php include('header.inc.php')?>
<div class="sb-slidebar sb-left">
    <a href="./">Home</a>
    <a href="./auth/">Auth</a>
    <a href="./registration/">Registration</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <h1 style="color:#DAA520; margin-left:600px">Welcome to WEB TODO.COM</h1>
    </div>
</div>
<?php include ("footer.inc.php")?>
</body>

</html>