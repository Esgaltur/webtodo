<!DOCTYPE html>
<html>
<head>
<?php include_once("../../webtodoConst.php")?>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login page</title>
    <link rel="stylesheet" href="../css/authStyle.css">
    <link rel="stylesheet" href="../css/loginForm.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <?php


    if (isset($_REQUEST["act"]))
    {

        if(isset($_SESSION["bad"])){
            if (strcasecmp($_SESSION["bad"],$_REQUEST["act"])==0) {
                $badLogin = "Your login or password is not correct!";
                unset($_SESSION);
            }
        }
        if(isset($_SESSION["End"])) {
            if (strcasecmp($_SESSION["End"], $_REQUEST['act'])==0) {
                $Msg = "Your logout has been successful";
                unset($_SESSION);
            }
        }
    }
    ?>
</head>
<body>
<header>
    <h3 class="header">WEBTODO.com</h3>
</header>
<div class="container">
    <div class="main-content">
        <div class="content">
            <div class="content-body">
                <form action="../../src/servlets/auth/auth.php"  method="post" id="login">
                    <h1>Log In</h1>
                    <fieldset id="inputs">
                        <input id="username" type="text" placeholder="Username" name="login" autofocus required>
                        <input id="password" type="password" placeholder="Password" name="password" required>
                    </fieldset>
                    <fieldset id="actions">
                        <input type="submit" id="submit" value="Log in">
                        <a href="fgtpass.php">Forgot your password?</a><a href="../registration/">Register</a>
                    </fieldset>
                    <fieldset>
                       <?php
                        if(isset($badLogin))
                       echo "<span style='color:red; margin-left:50px;'>".$badLogin."</span>";
                        elseif(isset($Msg))
                       echo "<span style='color:green; margin-left:50px;'>".$Msg."</span>";
                       ?>
                    </fieldset>
                </form>
                <div id="back1"></div>
                <div id="back2"></div>
                <div id="back3"></div>
                <div id="back4"></div>
                <div id="back6"></div>

            </div>
        </div>
    </div>
</div>
<?php include ("../footer.inc.php")?>
</body>
</html>