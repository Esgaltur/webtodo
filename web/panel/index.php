<?php
namespace webtodo;
if(!isset($_SESSION['usrId']))
{
    header('Location:'.$_SERVER['DOCUMENT_ROOT'].'/webtodo/src/servlets/logout.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<?php

require_once "../../src/servlets/TODO/TodoListDAO.php";
$List = null;
$Msg = "";
    if(isset($_SESSION['usrId'])){
        $List  =  TodoListDAO::getAllListsByUsrId($_SESSION['usrId']);

    }
if(isset($_GET['act']))
{
    if(strcasecmp(md5('badid'),$_GET['act'])==0)
    {
     $Msg = 'Record Does not exists, try again';
    }
}
?>


    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>WebToDo.com</title>
    <!--Styles--->
    <link rel="stylesheet" href="../css/panel.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/footer.css">

    <!--Styles--->
    <!--JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../js/menu.js"></script>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();

                var menu = $('#menu')
                $('#menu-trigger').click(function(event){
                    event.preventDefault();

                    if (menu.is(":visible"))
                    {
                        menu.slideUp(400);
                        $(this).removeClass("open");
                    }

                    else
                    {
                        menu.slideDown(400);
                        $(this).addClass("open");
                    }
                });
            });
        }) (jQuery);
    </script>
    <!--JavaScript--->




</head>

<body>
<?php

include('../header.inc.php')
    ?>


<div class="sb-slidebar sb-left">
    <a href="../todo/">My Todo</a>
    <a href="../account/">Account</a>
    <a href="../../src/servlets/auth/logout.php">Log Out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>

    <div class="main-content">
        <table class='listTodo' border="1">

          <?php
          echo"<tr>
                <td>
                    <table>
                        <tr>
                          <td>
                             <a href='./addtodolist/'>Add new Todo List</a>
                          </td>
                        </tr>
                    </table>
                </td>
               </tr>";
          echo "<tr>
                  <th>List Name</th>
                  <th>Date of Creation</th>
                  <th>Show list</th>
               </tr>";
          try{
          if(isset($List)){
          while($row = $List->fetch_assoc()) {
              echo
                  "<tr>
                      <td>".$row["ListName"]."</td>
                      <td> ".$row["CreationDate"]."</td>
                      <td><a href='./todo/?tdlid=".$row["listID"]."&uid=".$_SESSION['usrId']."'>Show List</a></td>
                  </tr>";
              echo "<span style='color:red; margin-left:50%'>".$Msg."</span>";
          }
          }
          else
          {
              echo
                  "<tr>
                      <td></td>
                      <td>No data</td>
                      <td></td>
                  </tr>";
          }
          }
          finally{
              unset($List);
          }
          ?>
        </table>

    </div>

</div>

<!--/#header-->
<?php include ("../footer.inc.php")?>
</body>

</html>