<?namespace webtodo;
if(!isset($_SESSION['usrId']))
{
    header('Location:'.$_SERVER['DOCUMENT_ROOT'].'/webtodo/src/servlets/logout.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require_once "../../../src/servlets/TODO/TodoListDAO.php";
    require_once('../../../SharedFunctions.php');
    if(isset($_GET['tdlid'])&&isset($_GET['uid'])) {
        $ListId = StripAndTrim($_GET['tdlid']);
        $usrid = StripAndTrim($_GET['uid']);
        try {

            $dao = new \webtodo\TodoListDAO();
            $TodoList = $dao->getTodoListById($usrid, $ListId);

        } finally {
            unset($dao);
        }
    }
    ?>


    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>WebToDo.com</title>
    <!--Styles--->
    <link rel="stylesheet" href="../../css/panel.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../../css/footer.css">

    <!--Styles--->
    <!--JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../../js/menu.js"></script>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();

                var menu = $('#menu')
                $('#menu-trigger').click(function(event){
                    event.preventDefault();

                    if (menu.is(":visible"))
                    {
                        menu.slideUp(400);
                        $(this).removeClass("open");
                    }

                    else
                    {
                        menu.slideDown(400);
                        $(this).addClass("open");
                    }
                });
            });
        }) (jQuery);
    </script>
    <!--JavaScript--->




</head>

<body>
<?php

include('../../header.inc.php')
?>


<div class="sb-slidebar sb-left">
    <a href="../../panel">My Todos</a>
    <a href="../../account/">Account</a>
    <a href="../../../src/servlets/auth/logout.php">Log Out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>

    <div class="main-content">
        <table class='listTodo' border="1">

            <?php
            if(isset($TodoList)) {
                try {
                    $TodoList->printList();

                } finally {
                    unset($TodoList);
                }
            }
            else{echo"<h1 style='margin-left:50%;color:red'>No Data to Show</h1>";}
            ?>
        </table>

    </div>

</div>

<!--/#header-->
<?php include ("../../footer.inc.php")?>
</body>

</html>