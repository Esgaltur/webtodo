<?php
namespace webtodo;
include_once ('../../../../SharedFunctions.php');
if(!isset($_SESSION['usrId']))
{
  header('Location:'.$_SERVER['DOCUMENT_ROOT'].'/webtodo/src/servlets/logout.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php

    require_once "../../../../src/servlets/TODO/TodoListDAO.php";
    $List = null;
    $Msg = "";
    if(isset($_GET['tdlid'])){
     $tdlid = StripAndTrim($_GET['tdlid']);
    }
    ?>


    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>WebToDo.com</title>
    <!--Styles--->
    <link rel="stylesheet" href="../../../css/panel.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../../../css/footer.css">

    <!--Styles--->
    <!--JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../../../js/menu.js"></script>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();

                var menu = $('#menu')
                $('#menu-trigger').click(function(event){
                    event.preventDefault();

                    if (menu.is(":visible"))
                    {
                        menu.slideUp(400);
                        $(this).removeClass("open");
                    }

                    else
                    {
                        menu.slideDown(400);
                        $(this).addClass("open");
                    }
                });
            });
        }) (jQuery);
    </script>
    <!--JavaScript--->




</head>

<body>
<?php

include('../../../header.inc.php')
?>


<div class="sb-slidebar sb-left">
    <a href="../../todo/">My Todo</a>
    <a href="../../../account/">Account</a>
    <a href="../../../../src/servlets/auth/logout.php">Log Out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>

    <div class="main-content">
        <h1 style="margin-left:50%">Adding new Todo</h1>
        <form style="margin-left:50%" action="../../../../src/servlets/TODO/addtodo.php" method="post">
            <input type="text" name="TodoName" placeholder="Todo name"><br>
            <textarea name="text" placeholder="Enter text/points to do">

            </textarea><br>
                <input type="date" name="from" placeholder="from"><br>
                <input type="date" name="to" placeholder="to"><br>
                <select name="category" >
                    <option value="1">Other</option>
                </select><br>
                <input type="text" name="level" placeholder="level"><br>
                <input type="text" name="todolistid" hidden="hidden" value="<?php echo $tdlid?>">
                <input type="text" name="usrid" hidden="hidden" value="<?php echo $_SESSION['usrId']?>">
            <input type="submit" value="Insert new Todo to list">

        </form>
        <?php /*<select id="cmbMake" name="Make" >
     <option value="0">Select Manufacturer</option>
     <option value="1">--Any--</option>
     <option value="2">Toyota</option>
     <option value="3">Nissan</option>
  </select>*/?>
        <?php echo "<span style='color:red;margin-left:50%'>".$Msg."</span>"?>

    </div>

</div>

<!--/#header-->
<?php include ("../../../footer.inc.php")?>
</body>

</html>