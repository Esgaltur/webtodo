<footer>
    <ul>

        <li>
            <ul>
                <li>
                    <hr>
                    <a class="logo" href="."><i>&copy; 2015</i>WEBTODO.com</a></li>
            </ul>
        </li>

        <li>
            <ul>
                <li><a href="#">First</a></li>
                <li><a href="#">Second</a></li>
                <li><a href="#">Third</a></li>
            </ul>
        </li>
        <li>
            <ul>
                <li><a href="mailto:no-reply@esgaltur.com">Email</a></li>
                <li><a href="#">About</a></li>
                <li><a href="callto:+420777449973">+420777449973</a></li>
            </ul>
        </li>
        <li>
            <ul>
                <li><a href="../auth/">Login Area</a></li>
                <li><a href="../registration.jsp">Registration</a></li>
                <li><a href="mailto:no-reply@esgaltur.com">FAQ</a></li>
            </ul>
        </li>
    </ul>
</footer>