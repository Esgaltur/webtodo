-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 09 2015 г., 10:54
-- Версия сервера: 5.6.14
-- Версия PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `webtodo.com`
--

-- --------------------------------------------------------

--
-- Структура таблицы `todo`
--

CREATE TABLE IF NOT EXISTS `todo` (
  `RecId` int(11) NOT NULL AUTO_INCREMENT,
  `usrId` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `level` smallint(5) NOT NULL,
  `category` smallint(6) NOT NULL,
  `from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TodoListId` int(11) NOT NULL,
  PRIMARY KEY (`RecId`),
  KEY `usrId` (`usrId`),
  KEY `TodoListId` (`TodoListId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Очистить таблицу перед добавлением данных `todo`
--

TRUNCATE TABLE `todo`;
--
-- Дамп данных таблицы `todo`
--

INSERT INTO `todo` (`RecId`, `usrId`, `name`, `text`, `to`, `level`, `category`, `from`, `TodoListId`) VALUES
  (1, 1, 'Купить', 'Молоко\r\nХлеб\r\nПомидоры', '2015-09-09 22:00:00', 5, 6, '2015-09-08 22:00:00', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `todolist`
--

CREATE TABLE IF NOT EXISTS `todolist` (
  `listID` int(11) NOT NULL AUTO_INCREMENT,
  `ListName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`listID`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Очистить таблицу перед добавлением данных `todolist`
--

TRUNCATE TABLE `todolist`;
--
-- Дамп данных таблицы `todolist`
--

INSERT INTO `todolist` (`listID`, `ListName`, `CreationDate`, `userId`) VALUES
  (1, 'Покупки', '2015-09-08 23:36:15', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `userauth`
--

CREATE TABLE IF NOT EXISTS `userauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `lastSuccLog` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Очистить таблицу перед добавлением данных `userauth`
--

TRUNCATE TABLE `userauth`;
--
-- Дамп данных таблицы `userauth`
--

INSERT INTO `userauth` (`id`, `login`, `password`, `lastSuccLog`) VALUES
  (1, 'admin', 'admin', '2015-09-06 18:19:02'),
  (2, 'John', '1233212334', '0000-00-00 00:00:00'),
  (3, 'DOE', '1233212334', '2015-08-23 10:23:43');

-- --------------------------------------------------------

--
-- Структура таблицы `userdata`
--

CREATE TABLE IF NOT EXISTS `userdata` (
  `usrId` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DBirth` date NOT NULL,
  PRIMARY KEY (`usrId`),
  KEY `usrId` (`usrId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Очистить таблицу перед добавлением данных `userdata`
--

TRUNCATE TABLE `userdata`;
--
-- Дамп данных таблицы `userdata`
--

INSERT INTO `userdata` (`usrId`, `name`, `email`, `DBirth`) VALUES
  (1, 'Dmitriy Sosnovich', 'admin@esgaltur.com', '1991-09-22');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `todo`
--
ALTER TABLE `todo`
ADD CONSTRAINT `todo_ibfk_1` FOREIGN KEY (`usrId`) REFERENCES `userauth` (`id`);

--
-- Ограничения внешнего ключа таблицы `userdata`
--
ALTER TABLE `userdata`
ADD CONSTRAINT `userdata_ibfk_1` FOREIGN KEY (`usrId`) REFERENCES `userauth` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
